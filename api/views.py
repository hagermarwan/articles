from rest_framework import generics
from articles.models import Article,Comment
from users.models import CustomUser 
from .serializers import ArticleSerializer,UserSerializer,CommentSerializer
# Create your views here.

class ListArticle(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class DetailArticle(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

class ListComment(generics.RetrieveUpdateDestroyAPIView):
    queryset= Comment.objects.all()
    serializer_class = CommentSerializer

class DetailComment(generics.RetrieveUpdateDestroyAPIView):
    queryset= Comment.objects.all()
    serializer_class = CommentSerializer
        
class ListUser(generics.ListCreateAPIView):
    queryset= CustomUser.objects.all()
    serializer_class = UserSerializer

class DetailUser(generics.RetrieveUpdateDestroyAPIView):
    queryset= CustomUser.objects.all()
    serializer_class = UserSerializer
    
