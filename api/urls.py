from django.urls import path
from .views import ListArticle,DetailArticle,ListUser,DetailUser,DetailComment,ListComment

urlpatterns =[
    path("<int:pk>/", DetailArticle.as_view()),
    path("",ListArticle.as_view()),
    path("<int:pk>/", DetailComment.as_view()),
    path("",ListComment.as_view()),
    path("users/", ListUser.as_view()),
    path("users/<int:pk>/", DetailUser.as_view()),

    
]