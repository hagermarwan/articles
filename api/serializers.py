from rest_framework import serializers
from articles.models import Article,Comment
from users.models import CustomUser

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields= ("title", "body","date","author")

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields= ("article", "comment","auther")


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model= CustomUser
        fields= ("age","id","username")
